#include <iostream>
#include <bitset>
 
std::bitset<4> rotl_1(std::bitset<4> bits){
    
    return (bits<<1 | bits>>3);
}

std::bitset<4> rotl(std::bitset<4> bits, int rotate=1)
{
    for(int i=0; i < rotate; i++){
        bits = rotl_1(bits);
    }
    return bits;
}

int main()
{
	std::bitset<4> bits1{ 0b0001 };
	std::cout << rotl(bits1) << '\n';
 
	std::bitset<4> bits2{ 0b1001 };
	std::cout << rotl(bits2, 2) << '\n';
 
	return 0;
}
