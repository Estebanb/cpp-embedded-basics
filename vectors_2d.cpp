#include <iostream>
#include <vector>

using namespace std;

int main(){
    vector< vector<int> > vv{5, vector<int>(5, 2)};

    for(auto i: vv){
        for(auto j: i)
            cout<<j;
        cout<<'\n';
    }

}
