#include <iostream>
using namespace std;

class A{
    private:
        string a;
    public:
        A(){};
        A(string aa): a{aa}{};
        virtual void print() const{
            cout<<"class A"<<endl;
        };
};
class AB: public A{
    private:
        string ab;
    public:
        AB(string aa): ab{aa}{};
        virtual void print() const override{
            cout<<"class AB"<<endl;
        };
};
class AC: public A{
    private:
        string ac;
    public:
        AC(string aa): ac{aa}{};
        virtual void print() const override{
            cout<<"class AC"<<endl;
        };
};
/* The trick here is that we can use only one function that appoint to a virtual function in the parent
 * to call the method print. Magic occurs when the methods are virtual! with virtual functions is posible
 * to call the method of the son trough the parent.
 */

void a_print_caller(const A &a){
    a.print();
};

int main(){

    A a("a");
    a_print_caller(a);

    AB ab("a");
    a_print_caller(ab);

    AC ac("a");
    a_print_caller(ac);

}
