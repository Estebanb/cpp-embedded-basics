#include <iostream>
#include <vector>
#include <algorithm>

class Car{
    private:
        std::string _brand;
        std::string _model;
    public:
        Car(std::string brand, std::string model) : _brand{brand}, _model{model}{};

        friend std::ostream &operator << (std::ostream &os,const Car car);
        friend bool operator == (const Car &car1, const Car &car2);
        friend bool operator != (const Car &car1, const Car &car2);
        friend bool operator < (const Car &car1, const Car &car2);
};

bool operator == (const Car &car1, const Car &car2){
    return car1._model == car2._model && car1._brand == car2._brand;
};

bool operator != (const Car &car1, const Car &car2){
   return car1._model != car2._model && car1._brand != car2._brand;
};

bool operator < (const Car &car1, const Car &car2){
    if (car1._brand == car2._brand)
        return car1._model < car2._model;
    else 
        return car1._brand < car2._brand;
};


std::ostream &operator << (std::ostream &os, const Car car){
    os<<"model: "<<car._model<<"\t ,";
    os<<"brand: "<<car._brand<<'\n';
    return os;
};


int main(){
    std::vector<Car> v{
        { "Toyota", "Corolla" },
        { "Honda", "Accord" },
        { "Toyota", "Camry" },
        { "Honda", "Civic" }
    };

    std::sort(v.begin(), v.end());

    for(const auto i: v)
        std::cout<<i;

    return 0;
};
