#include <iostream>
#include <map>

using namespace std;

int main(){
    multimap<int, string> mp;
    mp.insert(make_pair(1, "uno"));
    mp.insert(make_pair(1, "uno-dos"));
    mp.insert(make_pair(1, "uno-tres"));
    mp.insert(make_pair(1, "uno-cuatro"));
    mp.insert(make_pair(2, "dos-uno"));
    mp.insert(make_pair(2, "dos-dos"));
    mp.insert(make_pair(2, "dos-tres"));

    cout<<"Original map: "<<endl;
    for(auto i: mp)
        cout<<"Key: "<<i.first<<" Value: "<<i.second<<endl;


    cout<<"Truncated multimap"<<endl;
    /* To get values in one range we can use the equal_range
     * this method of multipam return one par of iterators,
     * one from the start and other for the end
     */
    auto it = mp.equal_range(1);
    for(auto its=it.first; its!=it.second; its++)
        cout<<its->first<<its->second<<endl;
    return 0;
}
