#include <iostream>
#include <set>

using namespace std;

class Fruit{
    private:
        string name;
        int weight;
    public:
        Fruit(string n, int w): name{n}, weight{w}{};
        bool operator< (const Fruit &fruit) const{
            if(name == fruit.name)
                return weight<fruit.weight;
            return name < fruit.name;
        };
        friend ostream& operator<<(ostream &os, const Fruit &fruit){
            os<<"name: "<<fruit.name<<", weight: "<<fruit.weight;
            return os;
        };
};


int main(){
    set<Fruit> fruits;
    fruits.insert(Fruit("pera", 2));
    fruits.insert(Fruit("pera", 1));

    for(auto i: fruits)
        cout<<i<<endl;

    if(fruits.find(Fruit("pera", 2)) !=fruits.end())
        cout<<"existe"<<endl;

    if(fruits.count(Fruit("pera", 2)))
        cout<<"existe"<<endl;
}
