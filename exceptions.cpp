#include <iostream>
#include <vector>


void throwExceptionsBaby(){
    bool intexcept = false;
    bool charxcept = false;
    bool stringexcept = false;
    bool vectorexcept = true;

    if (intexcept)
        throw 8;
    if (charxcept)
        throw "static string c-style";
    if (stringexcept)
        throw std::string("String class message");
    if (vectorexcept)
        throw std::vector<int>{1,2};
}


int main(){
    try{
        throwExceptionsBaby();
    }catch(int e){
        std::cout << e <<'\n';
    }catch(const char *e){
        std::cout << e <<'\n';
    }catch(std::string &e){
        std::cout << e <<'\n';
    }catch(std::vector<int> &e){
        std::cout << e.at(0) <<'\n';
    }catch(char *e){
        std::cout << e <<'\n';
    }
};

