#include <iostream>
#include <set>

using namespace std;

int main(){
    set<int> setter;

    setter.insert(1);
    setter.insert(3);
    setter.insert(5);
    setter.insert(7);

    cout<<"Setters: "<<endl;
    for(auto i: setter)
        cout<<i<<endl;

    /*First way to find a number
     */
    if(setter.find(3) != setter.end())
        cout<<"exsists"<<endl;

    /* Other way to see if exsists
     */
    if(setter.count(3))
        cout<<"exsists"<<endl;
}
