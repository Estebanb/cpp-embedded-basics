#include <iostream>

using namespace std;

/* This is an abstract class, by itself it does nothing.
 * All their methods should be declareds in the derived classes
 */
class Animal{
    public:
        /* This functions are pure virtuals, should be defined before
         */
        virtual void speak()=0;
        virtual void run()=0;
};

class Dog: public Animal{
    public:
        virtual void speak() override final{
            cout<<"Wowf"<<endl;
        };
        virtual void run() override{
            cout<<"Running"<<endl;
        };
};

/* Abstract functions are usefulls to object slicing methods,
 * In this case if there is other kind of animal like Cat, you
 * can still using print_animals_info to print the virtual methods 
 * described in the abstract class
 */
void print_animals_info(Animal &animal){
    animal.speak();
    animal.run();
};

int main(){
    Dog dog;
    print_animals_info(dog);
    return 0;
}
