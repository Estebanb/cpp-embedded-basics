#include <iostream>

template<typename T>
class Point3d{
    private:
        T _x;
        T _y;
        T _z;
    
    public:
        void setValues(T x,T y,T z){_x=x, _y=y, _z=z;};
        Point3d(T x, T y, T z){setValues(x, y, z);};
        Point3d(){};
        bool isEqual(Point3d &point){
            return (_x==point._x && _y==point._y && _z==point._z);
        }
        friend bool operator == (const Point3d<T> &point1, const Point3d<T> &point2){
            return (point2._x==point1._x && point2._y==point1._y && point2._z==point1._z);
        }
};

int main()
{
    Point3d<int> point1;
    point1.setValues(1, 2, 3);
 
    Point3d<int> point2;
    point2.setValues(1, 2, 3);
 
    if (point1 == point2)
        std::cout << "point1 and point2 are equal\n";
    else
        std::cout << "point1 and point2 are not equal\n";
 
    Point3d<int> point3;
    point3.setValues(3, 4, 5);
 
    if (point1 == point3)
        std::cout << "point1 and point3 are equal\n";
    else
        std::cout << "point1 and point3 are not equal\n";
 
    return 0;
}
