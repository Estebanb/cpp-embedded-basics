#include <iostream>

class MyExcept1: public std::exception {
    /*We can implement override virtual function, this ensure that
     *we are overriding an existent virtual function and we are not
     *creating a new one.
     */
    public:
        virtual const char* what() const noexcept override {
            return "MyExcept1";
        };
};

class MyExcept2: public MyExcept1{
    public:
        virtual const char* what() const noexcept override {
            return "MyExcept2";
        };
};

class MyExcept3: public MyExcept2{
    /* Its possible to implement the keyword final, his task its
     * to dont allow the new derivated functions to override 
     * this final override of the virtual function
     */
    public:
        virtual const char* what() const noexcept override final{
            return "MyExcept3";
        };
};

class ThrowExcepts{
    public:
        void excepter1(){
            throw MyExcept1();
        };
        void excepter2(){
            throw MyExcept2();
        };
        void excepter3(){
            throw MyExcept3();
        };
};

int main(){
    /* MyExcept 1, 2 and 3 derivate from exception, they use virtual functions
     * This is an excelent case of polymorphism, because we implement a catcher
     * For the base class (std::excepion) and we can call the derived shows
     * functions, this allows us to implement one generic tach for all the excepts
    */ 
    ThrowExcepts te;
    try{
        te.excepter3();
    }catch(std::exception &e){
        std::cout<< "show msg: "<< e.what() << '\n';
    }
}
