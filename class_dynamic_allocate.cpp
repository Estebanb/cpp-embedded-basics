#include <iostream>

class Stack{
    private:
        int *stack;
        size_t size;
        size_t index{0};

    public:
        Stack(size_t stack_size): size(stack_size){
            stack = new int[size];
        };

        /*
        ~Stack(){
            delete[] stack;
        };
        */

        friend std::ostream& operator << (std::ostream &os, const Stack stack){
            os<<"size: "<<stack.size<<", index: "<<stack.index<<'\n';
            for(int i=0; i<static_cast<int>(stack.index); i++){
                os<<stack.stack[i]<<",";
            }
            os<<'\n';
            return os;
        }

        void push(int value){
            stack[index++] = value; 
        }

        int pop(){
            return stack[--index]; 
        }
};



int main(){
    Stack stack(10);
    std::cout<<stack;
    stack.push(1);
    std::cout<<stack;
    stack.push(4);
    stack.push(4);
    stack.push(4);
    stack.push(4);
    std::cout<<stack;
    stack.pop();
    std::cout<<stack;
}
