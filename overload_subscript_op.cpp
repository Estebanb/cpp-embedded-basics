#include <iostream>

class IntClass{
    private:
        int m_list[10];
    public:
        int& operator[] (int index);
};

int& IntClass::operator[] (int index){
    return m_list[index];
};


int main(){
    IntClass list;
    list[2] = 3; // set a value
    std::cout << list[2]; // get a value
 
    return 0;
};
