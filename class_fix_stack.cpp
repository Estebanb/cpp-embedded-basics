#include <iostream>

template<size_t Z>
class Stack{
    private:
        int s[Z];
        size_t index{0};

    public:
        void reset(){index=0;};
        bool push(int value)
        {
            if (index>=Z) return false;
            s[index]=value;
            index++;
            return true;
        };

        int pop(){
            return (s[(--index)-1]);
        };
		friend std::ostream& operator<<(std::ostream &os, const Stack &s){
            for (int i=0; i < static_cast<int>(s.index); i++){
                os<<s.s[i]<<",";
            };
			os<<'\n';
			return os;
		};
};


int main()
{
	Stack<10> stack;
	stack.reset();
 
	std::cout<<stack;
 
	stack.push(5);
	stack.push(3);
	stack.push(8);
	std::cout<<stack;
 
	stack.pop();
	std::cout<<stack;
 
	stack.pop();
	stack.pop();
 
	std::cout<<stack;
 
	return 0;
};
