#include <iostream>
#include <vector>
using namespace std;

void print1(){cout<<"print 1"<<endl;}
void print2(){cout<<"print 2"<<endl;}
void print3(){cout<<"print 3"<<endl;}
void print4(){cout<<"print 4"<<endl;}
void print5(){cout<<"print 5"<<endl;}
void print6(){cout<<"print 6"<<endl;}
void print7(){cout<<"print 7"<<endl;}


int main(){
    vector<void(*)()> funcvect{print1,print2,print3,print4,print5,print6,print7}; 

    for(auto i: funcvect)
        i();

    void (*punt_func)() = print1;
    punt_func();
}
