#include <iostream>
#include <stack>

using namespace std;

class Fruit{
    private:
        string name;
        int weight;
    public:
        Fruit(string n, int w):name{n}, weight{w} {};
        friend ostream& operator<<(ostream &os, const Fruit &fruit){
            os<<"name: "<<fruit.name<<" weight: "<<fruit.weight<<flush;
            return os;
        };

        ~Fruit(){
            //cout<<"Destructor Fruit de: "<<name<<endl;
        }
};

/* The stack implement a method call top, this method return
 * de element in the top.
 * The method pop dont return nothing, just drop the last element
 */

int main(){
    stack<Fruit> fruits;

    fruits.push(Fruit("pera", 2));
    fruits.push(Fruit("banana", 1));
    fruits.push(Fruit("morron", 5));

    cout<<"stack top :"<<fruits.top()<<endl;
    fruits.pop();
    cout<<"we pop one"<<endl;
    cout<<"stack top :"<<fruits.top()<<endl;

}

/* A lot of classes create  copyes from the objects, they implement shallow copies
 * If you allocate memory dynamically in some class its important to overload the 
 * constructor that make the copy to create a copy of the dynamically allocated 
 * variables.
 */
