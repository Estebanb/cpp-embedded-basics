#include <iostream>

class Digit{
    private:
        int num;
    public:
        Digit(int n) : num{n} {};
        Digit &operator++(); //Prefix
        Digit &operator--(); //Prefix

        Digit operator++(int); //Postfix
        Digit operator--(int); //Postfix

        friend std::ostream &operator<< (std::ostream &os, const Digit dig);
};


Digit &Digit::operator++(){
    (num>=9) ? num = 0 : num++;
    return *this;
};

Digit &Digit::operator--(){
    (num<=0) ? num = 9 : num--;
    return *this;
};

Digit Digit::operator++(int){
    Digit tmp(num);
    ++(*this);
    return tmp;
};

Digit Digit::operator--(int){
    Digit tmp(num);
    --(*this);
    return tmp;
};

std::ostream &operator<< (std::ostream &os, const Digit dig){
    os<<dig.num;
    return os;
};

int main(){
    Digit digit(5);
 
    std::cout << digit;
    std::cout << ++digit; // calls Digit::operator++();
    std::cout << digit++; // calls Digit::operator++(int);
    std::cout << digit;
    std::cout << --digit; // calls Digit::operator--();
    std::cout << digit--; // calls Digit::operator--(int);
    std::cout << digit;
 
};
