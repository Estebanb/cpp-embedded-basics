#include <iostream>

#define HeapSpaceSize 100

class LIFOAllocator { // provides behavior
    public: // of new/delete via
        LIFOAllocator(unsigned char* heapAddr, std::size_t heapSize) : heapBase(heapAddr),
                                                                       heapEnd(heapAddr+heapSize),
                                                                       heapTop(heapAddr) {}
        void* allocate(std::size_t sz); // shown shortly
        void deallocate(void* ptr, std::size_t sz); // ditto
    private:
        unsigned char * const heapBase;
        unsigned char * const heapEnd;
        unsigned char *heapTop;
};

void* LIFOAllocator::allocate(std::size_t bytes){
    if (bytes == 0) bytes = 1;
    while (true) {
        if (heapTop + bytes <= heapEnd) { // overflow?
            unsigned char *pMem = heapTop; // alignment?
            std::cout<<"heapTop:"<<(int *)heapTop<<std::endl;
            heapTop += bytes;
            std::cout<<"heapTop:"<<(int *)heapTop<<std::endl;
            return pMem;
    }
    std::new_handler currentHandler = std::get_new_handler();
    if (currentHandler) currentHandler();
    else throw std::bad_alloc();
    }
}

void LIFOAllocator::deallocate(void *ptr, std::size_t size){
    if (ptr == nullptr) return;
    if (heapTop != static_cast<unsigned char*>(ptr) + size) {
        std::cout<<"Heap data structures are invalid! reboot()"<<std::endl;
        // either client usage error or heap-related data structures are invalid
        //Log the problem, then call exit or abort or restart/reboot the system.
    }
    heapTop -= size;
}

class Widget {
    public:
        void printHelloWorld();
        void* operator new(std::size_t bytes);
        void operator delete(void *ptr, std::size_t size);
    private:
        int numero; //Define this Integer here expand the size of the class from 1bytes to 4 bytes.
        static const int numero_constante = 10; //This static const Integer don't expand the class size when instance.
                                                //This variable is ROMable, its to ReadOnlyMemory.
};

void Widget::printHelloWorld(){
    std::cout<<"Hello World"<<std::endl;
};

unsigned char heapSpace[HeapSpaceSize]; // memory for heap
LIFOAllocator customAllocator(heapSpace, // typically at global scope
                                  HeapSpaceSize);
void* Widget::operator new(std::size_t bytes)
{
    std::cout<<"Overload operator NEW to implement LIFO heap memory"<<std::endl;
    return customAllocator.allocate(bytes);
}

void Widget::operator delete(void *ptr, std::size_t size)
{
    std::cout<<"Overload operator DELETE to implement LIFO heap memory"<<std::endl;
    customAllocator.deallocate(ptr, size);
}

int main(){
    Widget *w = new Widget();
    Widget *a = new Widget();
    w->printHelloWorld();
    delete a;       //We implement a LIFO, so we should delete first a and then w.
    delete w;       //In other way memory struct will be corrupt and a interruption will be raised
    return 0;
}
