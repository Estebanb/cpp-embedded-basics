#include <iostream>
#include <exception>

class MyCustomException: public std::exception{
    public:
        virtual const char* what() const noexcept{
            return "Custom show in MyCustomException";
        };
};

class SomeClass{
    public:
        void throwException(){
            throw MyCustomException();
        };
};

int main(){
    SomeClass some;
    try{
        some.throwException();
    }catch(MyCustomException &e){
        std::cout<<e.what()<<'\n';
    }
};

