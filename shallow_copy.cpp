#include <iostream>

using namespace std;

class Persona{
    private:
        string name;
        int age;
    public:
        /* Default constructor
         */
        Persona(string n, int a): name{n}, age{a}{};
        /* friend overload for << opetator, used in cout<<class
         */
        friend ostream& operator<<(ostream &os, Persona &persona){
            os<<"name: "<<persona.name<<", age: "<<persona.age;
            return os;
        };
        /* Overload operator = used to "copy instances content"
         */
        const Persona& operator=(const Persona &persona){
            cout<<"overload operator ="<<endl;
            name = persona.name;
            age = persona.age;
            return *this;
        }

        /* This constructor is in wich we need to put the logic of the deep copy
         * if we need to perform it. We need to perform it when we allocate dynamic
         * memory inside the class
         */
        Persona(const Persona &persona){
            cout<<"shallow copy constructor"<<endl;
            name = persona.name;
            age = persona.age;
            /*The asigment of this do the same as copy every member
             */
            *this = persona;
        }
};


int main(){
    cout<<"instanciate object in the common way"<<endl;
    Persona fiamma("fiamma", 27);
    cout<<fiamma<<endl;
    Persona esteban("esteban", 26);
    cout<<esteban<<endl;

    cout<<"asignment fiamma to esteban"<<endl; 
    esteban = fiamma;
    cout<<esteban<<endl;

    cout<<"using the copy constructor to create a carlos"<<endl;
    Persona carlos = fiamma;
    cout<<carlos<<endl;



}
