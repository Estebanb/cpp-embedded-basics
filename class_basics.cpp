#include <iostream>

template<typename T>
class IntPair{
    private:
        T _a;
        T _b;
    public:
        void print(){std::cout<<_a<<","<<_b<<'\n';};
        void set(T a, T b){_a = a; _b = b;};
        IntPair(T a, T b){set(a,b);};
        IntPair(){};
};

int main(){
    IntPair<int> p1;
	p1.set(1, 1); // set p1 values to (1, 1)
	
	IntPair<float> p2{ 2.1, 2.2 }; // initialize p2 values to (2, 2)
 
	p1.print();
	p2.print();
 
	return 0;

}
