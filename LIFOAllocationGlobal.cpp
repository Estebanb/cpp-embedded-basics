#include <iostream>

#define HeapSpaceSize 100

/*
In older version of GCC the operator deallocate couldn't support the argument size and was necessary to
put the size of the memory to allocate in the first byte of the allocated memory. (It is necessary to 
allocate = sizeof(object) + sizeof(sizeof(object)), now it is not necessary.
To run this example its needed to build with -lstdc++.
Ex:  
	gcc LIFOAllocationGlobal.cpp -lstdc++ -o LIFOAllocationGlobal
*/
class LIFOAllocator { // provides behavior
    public: // of new/delete via
        LIFOAllocator(unsigned char* heapAddr, std::size_t heapSize) : heapBase(heapAddr),
                                                                       heapEnd(heapAddr+heapSize),
                                                                       heapTop(heapAddr) {}
        void* allocate(std::size_t sz);
        void deallocate(void* ptr, unsigned long size); 
    private:
        unsigned char * const heapBase;
        unsigned char * const heapEnd;
        unsigned char *heapTop;
};

void* LIFOAllocator::allocate(std::size_t bytes){
    if (bytes == 0) bytes = 1;
    while (true) {
        if (heapTop + bytes <= heapEnd) { // overflow?
            unsigned char *pMem = heapTop; // alignment?
            heapTop += bytes;
            return pMem;
    }
    }
}

void LIFOAllocator::deallocate(void *ptr, unsigned long size){
    if (ptr == nullptr) return;
    if (heapTop != static_cast<unsigned char*>(ptr) + size) {
        std::cout<<"Heap data structures are invalid! reboot()"<<std::endl;
    }
    heapTop -= size;
}

class Widget {
    public:
        void printHelloWorld();
};

void Widget::printHelloWorld(){
    std::cout<<"Hello World"<<std::endl;
}

unsigned char heapSpace[HeapSpaceSize]; // memory for heap
LIFOAllocator customAllocator(heapSpace, // typically at global scope
                                  HeapSpaceSize);

void* operator new(std::size_t bytes) throw ()
{
    return customAllocator.allocate(bytes);
}

void operator delete(void *ptr, unsigned long size) throw ()
{
    customAllocator.deallocate(ptr, size); // note lack of size param
}

int main(){
    Widget *w = new Widget();
    Widget *a = new Widget();
    w->printHelloWorld();
    delete a;       //We implement a LIFO, so we should delete first a and then w.
    delete w;       //In other way memory struct will be corrupt and a interruption will be raised
    return 0;
}
