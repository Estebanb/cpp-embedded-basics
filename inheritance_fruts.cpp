#include <iostream>

class Fruit{
    private:
        std::string m_name;
        std::string m_color;
    public:
        Fruit(std::string name, std::string color): m_name{name}, m_color{color} {};
        const std::string &get_color(){return m_color;};
        const std::string &get_name(){return m_name;};
};

class Apple: public Fruit{
    private:
        double m_fiber;
    public:
        Apple(std::string name, std::string color, double fiber) : Fruit{name, color}, m_fiber{fiber} {};
        friend std::ostream &operator << (std::ostream &os, Apple apple){
            os<<"Apple("<<apple.get_name()<<" ,"<<apple.get_color()<<" ,"<<apple.m_fiber<<")";
            return os;
        };
};

class Banana: public Fruit{
    public:
        Banana(std::string name, std::string color) : Fruit{name, color} {};
        friend std::ostream &operator << (std::ostream &os, Banana banana){
            os<<"Banana("<<banana.get_name()<<" ,"<<banana.get_color()<<")";
            return os;
        };
};


int main()
{
	const Apple a{ "Red delicious", "red", 4.2 };
	std::cout << a << '\n';
 
	const Banana b{ "Cavendish", "yellow" };
	std::cout << b << '\n';
 
	return 0;
}
