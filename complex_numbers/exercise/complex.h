#ifndef COMPLEX_H
#define COMPLEX_H

#include <iostream>
using namespace std;
namespace custom_complex{

class Complex{
    private:
        double real;
        double imaginary;
    public:
        Complex();
        Complex(double r, double i);
        Complex(const Complex &complex);
        friend ostream& operator<<(ostream &os, const Complex &complex);
        const Complex& operator=(const Complex complex);
        friend Complex operator+(const Complex &c1, const Complex &c2);
        double get_real() const {return real;};
        double get_imaginary() const {return imaginary;};
};


}; // end namespace custom_complex

#endif //COMPLEX_H
