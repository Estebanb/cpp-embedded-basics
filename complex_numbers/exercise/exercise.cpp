#include "complex.h"
using namespace custom_complex; 

int main(){
    Complex c1{0,0};
    Complex c2{1,1};
    cout<<c1<<endl;
    Complex c3 = c2;
    cout<<c3<<endl;
    c1=c2;
    cout<<c1<<endl;
    c2 = c1+c3;
    cout<<c2;
    cout<< c1+c2+c2+c3+c1 <<endl;
    return 0;
};
