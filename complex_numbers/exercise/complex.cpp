#include "complex.h"


namespace custom_complex{

Complex::Complex(double r, double i): real{r}, imaginary{i}{};
Complex::Complex(const Complex &complex){
    cout<<"Copy constructor"<<endl;
    real = complex.real;
    imaginary = complex.imaginary;
    /* If we use *this = complex, we call recursively this function.
     * its pretty funny :P
     */
};

ostream& operator<<(ostream &os, const Complex &complex){
    os<<"Real: "<<complex.real<<", Imag: "<<complex.imaginary;
    return os; 
};

Complex operator+(const Complex &c1, const Complex &c2){
    return Complex(c1.real+c2.real, c1.imaginary+c2.get_imaginary());
};

const Complex& Complex::operator=(const Complex complex){
    real = complex.real;
    imaginary = complex.imaginary;
    return *this;
};
}; // end namespace custom_complex
