#include <iostream>
using namespace std;

template<class T>
class ring{
    private:
        int m_size;
        T *m_buff;
        int m_pos;
    public:
        class iterator;
    public:
        ring(int size): m_size{size}, m_buff{NULL}, m_pos{0}{
            m_buff = new T[size];
        };
        void add(T value){
            m_buff[m_pos++] = value;
            if (m_pos >= m_size)
                m_pos = 0;
        }
        iterator begin(){
            return iterator(0, *this);
        };
        iterator end(){
            return iterator(m_size, *this);
        };
        T& get(int pos){
            return m_buff[pos];
        };

};

template<class T>
class ring<T>::iterator{
    private:
        int m_pos;
        ring *m_ring;
    public:
        iterator(int pos, ring &r): m_pos{pos}, m_ring{&r} {};
        bool operator!=(const iterator &it){
            return m_pos != it.m_pos;
        };
        iterator& operator++(int){
            m_pos++;
            return *this;
        };
        iterator& operator++(){
            m_pos++;
            return *this;
        };
        T& operator*(){
            return m_ring->get(m_pos);
        }
};


int main(){
    ring<string> r(3);

    r.add("One");
    r.add("Two");
    r.add("Three");
    r.add("Four");

    //C++ 98 old style iterator:
    for(ring<string>::iterator it=r.begin(); it!=r.end(); it++)
        cout<<*it;

    cout<<endl;

    //New C++ 11 style
    for(auto i: r)
        cout<<i;

    return 0;
}
