#include <iostream>
#include <fstream>
using namespace std;

int main(){
    string filename{"test.txt"};
    ifstream file(filename);

    if(!file.is_open())
        return 1;

    string line;

    while(getline(file, line)){
       string name = line.substr(0, line.find(':'));
       string age = line.substr(line.find(':'), line.size());
       cout<<"(name: "<<name<<"), (age: "<<age<<")"<<'\n';
    }
    file.close();
}
