#include <iostream>
#include <fstream>

using namespace std;

/* This struct shouldnt be writed and readed from the file without the pragma
 * pack, (wich force de precopilator to dont pack the structure, to keep it 
 * aligned) but it works :/ :O 
 */

//#pragma pack(push, 1)
struct Person{
    char name[50];
    int age;
    int weight;
};
//#pragma pack(pop)


int main(){
    Person person{"pedro", 12, 55};
    string fileName = "person.bin";
    ofstream file(fileName, ios::binary);
    if(!file.is_open())
        return 1;

    /*Reinterpret_cast is used to cast pointers
     */
    file.write(reinterpret_cast<char*>(&person), sizeof(person));
    file.close();

    
    Person person1;
    ifstream file1(fileName, ios::binary);
    if(!file1.is_open())
        return 1;

    /*Reinterpret_cast is used to cast pointers
     */
    file1.read(reinterpret_cast<char*>(&person1), sizeof(person1));
    file1.close();
    
    cout<<person1.name<<" ,"<<person1.age<<" ,"<<person.weight<<endl;

}
