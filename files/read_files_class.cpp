#include <iostream>
#include <fstream>
#include <vector>
using namespace std;


class CantOpenFile: public exception{
    public:
        virtual const char* what() const noexcept{
            return "It is not possible to open the file";
        };
};

class Person{
    private:
        string m_name;
        unsigned int m_age;
    public:
        Person(string name, unsigned int age): m_name{name}, m_age{age}{};
        friend ostream& operator<<(ostream &os, const Person &person){
            os<<"(name: "<<person.m_name<<"), (age: "<<person.m_age<<")"<<'\n';
            return os;
        }
};

class People{
    private:
        vector<Person> m_people;
    public:
        void load_people_from_file(string filename){
            ifstream file(filename);

            if(!file.is_open())
                throw CantOpenFile();

            string line;

            while(getline(file, line)){
               string name = line.substr(0, line.find(':'));
               int age = stoi(line.substr(line.find(':')+1, line.size()));
               m_people.push_back(Person(name, age));
            }
            file.close();   
        };
        friend ostream& operator<<(ostream &os, const People &people){
            for(auto person : people.m_people)
                os<<person;
            return os;
        };
};


int main(){
    string filename{"test.txt"};
    People people;
    people.load_people_from_file(filename);
    cout<<people<<endl;
}
