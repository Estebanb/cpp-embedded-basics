#include <iostream>
#include <map>

using namespace std;

class Person{
    private:
        string name;
        int age;
    public:
        Person(string n, int a): name{n}, age{a}{};
        Person(){};
        friend ostream& operator<<(ostream &os, const Person &person){
            os<<"Name: "<<person.name<<" Age: "<<person.age;
            return os;
        }
        /* The < its needed by the map to compare the keys, maps are sorted;
         */
        bool operator<(const Person &person) const{
            if(name == person.name)
                return age<person.age;
            else return name<person.name;
        };
};


int main(){
    /* Key with int and value with the class
     */
    map<int, Person> people;
    people[0] = Person("Esteban", 26);
    people[1] = Person("Fiamma", 27);
    people[2] = Person("Roberto", 25);

    for(auto p: people)
       cout<<"Key: "<<p.first<<" Value: "<<p.second<<endl; 

    /* Key with class and value number
     */
    map<Person, int> peoples;
    peoples[Person("Esteban", 26)] = 0;
    peoples[Person("Fiamma", 27)] = 1;
    peoples[Person("Roberto", 25)] = 2;

    for(auto p: peoples)
       cout<<"Key: "<<p.first<<" Value: "<<p.second<<endl; 

}
